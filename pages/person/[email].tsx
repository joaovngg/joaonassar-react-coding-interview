import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [editing, setEditing] = useState(false);
  const [user, setUser] = useState(data);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleChange = (value: string, key: string) => {
    const newData = { ...user, [key]: value };
    setUser(newData);
    save(newData);
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setEditing(!editing)}>
            Edit
          </Button>,
        ]}
      >
        {user && (editing ? <>
            <Input placeholder="Name" onChange={(e) => handleChange(e.target.value, 'name')} value={user.name}></Input>
            <Input placeholder="Gender" onChange={(e) => handleChange(e.target.value, 'gender')} value={user.gender}></Input>
            <Input placeholder="Phone" onChange={(e) => handleChange(e.target.value, 'phone')} value={user.phone}></Input>
            <Input placeholder="Birthday" onChange={(e) => handleChange(e.target.value, 'birthday')} value={user.birthday}></Input>
          </> :
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{user.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{user.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>

            <Descriptions.Item label="Birthday">{user.birthday}</Descriptions.Item>
          </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
